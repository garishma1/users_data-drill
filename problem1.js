//Q1 Find all users who are interested in playing video games.

//let dataUsers = require("./usersData")

function usersInterestedInPlayingVideoGames(data){
    
        let users_Interested_In_PlayingVideoGames=[]
        for(let user in data){
            for(let key in data[user]){
                    if(key==="interests" && data[user][key] .toString().includes("Playing Video Games") ){
                        users_Interested_In_PlayingVideoGames.push(user)
                    }
                }
            }
            return users_Interested_In_PlayingVideoGames
        }
        
//console.log(usersInterestedInPlayingVideoGames(dataUsers))
module.exports = usersInterestedInPlayingVideoGames