//Q3 Find all users with masters Degree.

//let dataUsers = require("./usersData")

function allUsersWithMastersDegree(data){
    let all_users_with_masters_degree = []
    for(let user in data){
        for(let key in data[user]){
            if(key==="qualification" && data[user][key]=="Masters"){
                all_users_with_masters_degree.push(user)

            }
        }
    }
    return all_users_with_masters_degree;
}
//console.log(allUsersWithMastersDegree(dataUsers))

module.exports = allUsersWithMastersDegree

